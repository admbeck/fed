# FED
Fast/File Encryption-Decryption

## Description
This command-line application allows users to encrypt and decrypt files using the AES (Advanced Encryption Standard) algorithm with a 256-bit key in CBC (Cipher Block Chaining) mode. The program uses the OpenSSL library for cryptographic operations.

## Features
* Encryption: Encrypt a file using AES-256 with a specified password.
* Decryption: Decrypt a previously encrypted file using the same password.
* File Format: Encrypted files have a "`.fed`" extension appended to the original file name.

## Usage
```bash
fed [-i] input_file [-p password] [-e | -d]
```
* `-i`: Specify the input file.
* `-p`: (optional) Provide the password for encryption or decryption.
* `-e`: Encrypt the file.
* `-d`: Decrypt the file.

## Examples

### Encrypt
```bash
fed -i input.txt -p very_secure -e
```
This command encrypts `input.txt` using the password "very_secure" and creates an encrypted file named `input.txt.fed`.

```bash
fed input.jpg
Enter Password:
```
A file can also be passed without -i key.

### Decrypt
```bash
fed -i input.txt.fed -d
Enter Password:
```
This command decrypts `input.txt.fed` using the password "very_secure" and creates a decrypted file named `input.txt`.

```bash
fed input.jpg.fed
Enter Password:
```
A file can also be passed without -i key.

## Dependencies
* OpenSSL

## Compilation
To compile the program, use the following command:
```bash
gcc -o fed fed.c -lssl -lcrypto
```
Ensure that you have the OpenSSL library installed on your system.

## License
GPL3
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
