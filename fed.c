#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#define SALT_SIZE 16
#define ITERATIONS 10000
#define KEY_SIZE 32 // 256 bits

void
handleErrors(const char* message)
{
    fprintf(stderr, "Error: %s\n", message);
    exit(EXIT_FAILURE);
}

char*
getPassword()
{
    struct termios oldt, newt;
    printf("Enter Password: ");
    fflush(stdout);

    size_t bufferSize = 256;
    char*  password = malloc(bufferSize);

    if (!password) {
        handleErrors("Password memory allocation error");
    }

    size_t length = 0;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~ECHO;

    if (tcsetattr(STDIN_FILENO, TCSANOW, &newt) != 0) {
        handleErrors("Failed to set terminal attributes");
    }

    int c;
    while ((c = getchar()) != '\n' && c != EOF) {
        password[length++] = (char)c;

        // Resize the buffer if necessary
        if (length == bufferSize) {
            bufferSize *= 2;
            password = realloc(password, bufferSize);

            if (!password) {
                handleErrors("Password memory reallocation error");
            }
        }
    }

    // Remove newline character from the end
    if (length > 0 && password[length - 1] == '\n') {
        password[length - 1] = '\0';
    }
    printf("\n");

    if (tcsetattr(STDIN_FILENO, TCSANOW, &oldt) != 0) {
        handleErrors("Failed to restore terminal attributes");
    }

    return password;
}

void
deriveKey(const char* password, unsigned char* salt, unsigned char* key)
{
    PKCS5_PBKDF2_HMAC_SHA1(
      password, strlen(password), salt, SALT_SIZE, ITERATIONS, KEY_SIZE, key);
}

void
encryptFile(const char* inputFileName,
            const char* outputFileName,
            const char* password)
{
    FILE* inputFile = fopen(inputFileName, "rb");
    FILE* outputFile = fopen(outputFileName, "wb");

    if (!inputFile) {
        handleErrors("Failed to open input file for reading");
    }

    if (!outputFile) {
        fclose(inputFile);
        handleErrors("Failed to open output file for writing");
    }

    EVP_CIPHER_CTX* ctx;
    // Generate a random salt
    unsigned char salt[SALT_SIZE];
    if (RAND_bytes(salt, sizeof(salt)) != 1) {
        fclose(inputFile);
        fclose(outputFile);
        handleErrors("Failed to generate random salt");
    }

    unsigned char key[KEY_SIZE], iv[16];
    deriveKey(password, salt, key);

    // Dynamically allocate buffer based on file size
    fseek(inputFile, 0, SEEK_END);
    long fileSize = ftell(inputFile);
    fseek(inputFile, 0, SEEK_SET);
    unsigned char* buffer = malloc(fileSize);

    if (!buffer) {
        fclose(inputFile);
        fclose(outputFile);
        handleErrors("Memory allocation error");
    }

    int bytesRead, encryptedLength, finalEncryptedLength;

    OpenSSL_add_all_algorithms();

    ctx = EVP_CIPHER_CTX_new();

    if (!ctx) {
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to create EVP cipher context");
    }

    if (!EVP_BytesToKey(EVP_aes_256_cbc(),
                        EVP_sha256(),
                        NULL,
                        (unsigned char*)password,
                        strlen(password),
                        1,
                        key,
                        iv)) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to derive key and IV");
    }

    fwrite(salt, 1, sizeof(salt), outputFile);

    if (EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) != 1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to initialize encryption");
    }

    bytesRead = fread(buffer, 1, fileSize, inputFile);

    if (EVP_EncryptUpdate(ctx, buffer, &encryptedLength, buffer, bytesRead) !=
        1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Encryption update error");
    }

    fwrite(buffer, 1, encryptedLength, outputFile);

    if (EVP_EncryptFinal_ex(ctx, buffer, &finalEncryptedLength) != 1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Encryption finalization error");
    }

    fwrite(buffer, 1, finalEncryptedLength, outputFile);

    EVP_CIPHER_CTX_free(ctx);

    fclose(inputFile);
    fclose(outputFile);
    free(buffer);

    printf("Encryption completed successfully.\n");
}

void
decryptFile(const char* inputFileName,
            const char* outputFileName,
            const char* password)
{
    FILE* inputFile = fopen(inputFileName, "rb");
    FILE* outputFile = fopen(outputFileName, "wb");

    unsigned char salt[SALT_SIZE];
    if (fread(salt, 1, sizeof(salt), inputFile) != sizeof(salt)) {
        fclose(inputFile);
        fclose(outputFile);
        handleErrors("Failed to read salt from input file");
    }

    // Ensure the file pointers are correctly positioned
    if (!outputFile) {
        fclose(inputFile);
        handleErrors("Failed to open output file for writing");
    }

    // Extract the salt from the input file
    EVP_CIPHER_CTX* ctx;
    unsigned char   key[KEY_SIZE], iv[16];

    // Derive the key using PBKDF2
    deriveKey(password, salt, key);

    // Dynamically allocate buffer based on file size
    fseek(inputFile, 0, SEEK_END);
    long fileSize = ftell(inputFile);
    fseek(
      inputFile, SALT_SIZE, SEEK_SET); // Correct positioning after reading salt
    unsigned char* buffer = malloc(fileSize);

    if (!buffer) {
        fclose(inputFile);
        fclose(outputFile);
        handleErrors("Memory allocation error");
    }

    int bytesRead, decryptedLength, finalDecryptedLength;

    OpenSSL_add_all_algorithms();

    ctx = EVP_CIPHER_CTX_new();

    if (!ctx) {
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to create EVP cipher context");
    }

    if (!EVP_BytesToKey(EVP_aes_256_cbc(),
                        EVP_sha256(),
                        NULL,
                        (unsigned char*)password,
                        strlen(password),
                        1,
                        key,
                        iv)) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to derive key and IV");
    }

    if (EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv) != 1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Failed to initialize decryption");
    }

    bytesRead = fread(
      buffer, 1, fileSize - SALT_SIZE, inputFile); // Read the rest of the file

    if (EVP_DecryptUpdate(ctx, buffer, &decryptedLength, buffer, bytesRead) !=
        1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Decryption update error");
    }

    fwrite(buffer, 1, decryptedLength, outputFile);

    if (EVP_DecryptFinal_ex(ctx, buffer, &finalDecryptedLength) != 1) {
        EVP_CIPHER_CTX_free(ctx);
        fclose(inputFile);
        fclose(outputFile);
        free(buffer);
        handleErrors("Decryption finalization error");
    }

    fwrite(buffer, 1, finalDecryptedLength, outputFile);

    EVP_CIPHER_CTX_free(ctx);

    fclose(inputFile);
    fclose(outputFile);
    free(buffer);

    printf("Decryption completed successfully.\n");
}

int
main(int argc, char* argv[])
{
    if (argc < 2 || argc > 6) {
        fprintf(
          stderr, "Usage: %s -i input_file [-p password] [-e | -d]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* inputFileName = NULL;
    const char* outputFileName = NULL;
    char*       password = NULL;
    int         encrypt = 0, decrypt = 0;

    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "-i") == 0) {
            inputFileName = argv[++i];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[++i];
        } else if (strcmp(argv[i], "-e") == 0) {
            encrypt = 1;
        } else if (strcmp(argv[i], "-d") == 0) {
            decrypt = 1;
        } else {
            inputFileName = argv[i];
        }
    }

    if (encrypt || decrypt) {
        // If -p is not specified, get the password
        if (!password) {
            password = getPassword();
        }
    } else {
        // Determine the action based on the absence of -e and -d options
        const char* extension = strstr(inputFileName, ".fed");
        if (extension != NULL && strcmp(extension, ".fed") == 0) {
            decrypt = 1;
        } else {
            encrypt = 1;
        }

        // If -p is not specified, get the password
        if (!password) {
            password = getPassword();
        }
    }

    if (!inputFileName || (encrypt && decrypt) || (!encrypt && !decrypt)) {
        fprintf(stderr, "Invalid arguments.\n");
        fprintf(
          stderr, "Usage: %s -i input_file [-p password] [-e | -d]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char*  tempPassword = NULL;
    size_t passwordMaxLength = 256;
    tempPassword = malloc(passwordMaxLength);

    if (!tempPassword) {
        handleErrors("Password memory allocation error");
    }

    if (password == NULL) {
        getPassword();
        password = tempPassword;
        free(tempPassword);
    }

    if (encrypt) {
        // Allocate memory for the modified string
        size_t outputLen =
          strlen(inputFileName) + 5; // ".fed" + null terminator
        char* tempOutputFileName = malloc(outputLen);
        if (!tempOutputFileName) {
            handleErrors("Memory allocation error");
        }
        snprintf(tempOutputFileName, outputLen, "%s.fed", inputFileName);

        // Convert to const pointer for final output
        outputFileName = tempOutputFileName;

        encryptFile(inputFileName, outputFileName, password);

        free(tempOutputFileName);
    } else if (decrypt) {
        if (strstr(inputFileName, ".fed") == NULL) {
            fprintf(stderr, "Invalid file format for decryption.\n");
            exit(EXIT_FAILURE);
        }

        // Allocate memory for the modified string
        size_t outputLen =
          strlen(inputFileName) - 4 + 1; // ".fed" + null terminator
        char* tempOutputFileName = malloc(outputLen);
        if (!tempOutputFileName) {
            handleErrors("Memory allocation error");
        }
        snprintf(tempOutputFileName, outputLen, "%s", inputFileName);

        // Convert to const pointer for final output
        outputFileName = tempOutputFileName;

        decryptFile(inputFileName, outputFileName, password);

        free(tempOutputFileName);
    }

    return 0;
}
